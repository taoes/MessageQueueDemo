# 部署RabbitMQ服务器
个人使用Docker部署，因此这里推荐使用Docker部署MQ服务器，命令如下:


```bash
docker run --name rabbitmq-5672 -p 5672:5672 -p 15672:15672  -d macintoshplus/rabbitmq-management
```



# 运行消费者

执行代码`com.zhoutao123.demo.rabbitmq.consume.ConsumeApplication` 观察命令行输出,这里可以运行多个消费者，以观察多个消费者的效果.



# 运行生产者

执行代码`com.zhoutao123.demo.rabbit.provide.ProvideApplication` 此项目中部署了定时任务,其会定时的推送消息，观察控制台输出记录。


# 可视化管理界面

若你使用了docker部署RabbitMq,那么你可以打开浏览器，输入`http://localhost:15672` 进入RabbitMQ的web管理界面，观察消息，队列，交换机等信息,默认账户和密码均是guest。
