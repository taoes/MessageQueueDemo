package com.zhoutao123.demo.rabbitmq.consume;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component

public class RabbitHandle {


  @RabbitListener(queues = "example")
  public void process(String hello, Channel channel, Message message) throws IOException, InterruptedException {
    System.out.println("3号接收到消息:" + hello);
    if (Integer.valueOf(hello) % 3 == 0) {
      System.out.println("丢弃消息");
      channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
    }
    channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
  }
}
