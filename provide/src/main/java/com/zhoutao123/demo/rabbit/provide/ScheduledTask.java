package com.zhoutao123.demo.rabbit.provide;


import com.zhoutao123.demo.rabbit.provide.ribbitmq.RabbitMessageSend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class ScheduledTask {

  @Autowired
  private RabbitMessageSend send;

  @Scheduled(cron = "0/2 * * * * ?")
  public void sendMessage() {
    send.send();
  }
}
