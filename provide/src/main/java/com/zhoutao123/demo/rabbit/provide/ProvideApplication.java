package com.zhoutao123.demo.rabbit.provide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ProvideApplication {

  public static void main(String[] args) {
    SpringApplication.run(ProvideApplication.class, args);
  }

}

