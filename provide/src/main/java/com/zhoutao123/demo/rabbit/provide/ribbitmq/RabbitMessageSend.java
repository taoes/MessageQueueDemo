package com.zhoutao123.demo.rabbit.provide.ribbitmq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

@Configuration
public class RabbitMessageSend implements RabbitTemplate.ReturnCallback, RabbitTemplate.ConfirmCallback {


  RabbitTemplate rabbitTemplate;

  @Autowired
  public RabbitMessageSend(RabbitTemplate rabbitTemplate) {
    this.rabbitTemplate = rabbitTemplate;

  }

  private static int i = 0;

  public void send() {
    send(String.valueOf(new Date().getTime()));
  }

  public void send(String content) { //System.out.println("发送数据:" + i++);
    this.rabbitTemplate.setReturnCallback(this);
    this.rabbitTemplate.setConfirmCallback(this);
    this.rabbitTemplate.convertAndSend("example", ++i);
  }

  @Override
  public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
    System.out.println("sender return success" + message.toString() + "===" + i + "===" + replyText + "====" + exchange + "===" + routingKey);
  }

  @Override
  public void confirm(CorrelationData correlationData, boolean ack, String cause) {
    if (!ack) {
      System.out.println(String.format("消息发送失败: %s - %s", cause, correlationData.toString()));
    } else {
      System.out.println("消息发送成功");
    }
  }
}
